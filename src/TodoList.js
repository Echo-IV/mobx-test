import React, { useContext, useEffect } from 'react';
import { observer } from 'mobx-react-lite';
import { TodoStoreContext } from './stores/TodoStore';

export const TodoList = observer(() => {

  const TodoStore = useContext(TodoStoreContext);

  useEffect(() => {
    TodoStore.fetchTodos()
  }, [])

  const { todos, completedTodos } = TodoStore;

  return (
    <div>
      {todos.map(todo => <><div>{todo.title}</div></>)}
      <span>There are {completedTodos} todo(s) completed</span>
    </div>
  )
})



export default TodoList;
