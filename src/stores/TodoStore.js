import { observable, computed, flow, configure } from 'mobx';
import { createContext } from 'react';

import config from '../config';

configure({ enforceActions: "always" })

class TodoStore {
  @observable todos = [];

  @computed get completedTodos() {
    return this.todos.filter(
      todo => todo.completed
    ).length
  }

  setTodos = (todos) => {
    this.todos = todos
  }

  fetchTodos = flow(function* () {
    try {
      const response = yield fetch(`${config.root}/todos?_limit=5`)
      const todos = yield response.json()

      this.setTodos(todos)

    } catch (error) {
      console.log(error);
    }
  })
}

export const TodoStoreContext = createContext(new TodoStore())